package com.eb03.dimmer;

import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Classe de mise en forme des trames a envoyer
 */
public class FrameProcessor {
    private final static byte TAIL = 0x04;
    private final static byte HEADER = 0x05;
    private final static byte ESC = 0x06;
    private byte[] mLength={0,0};


    private byte mControl;
    private List<Byte> mFrame = new LinkedList<Byte>();


    public FrameProcessor() {



    }

    /**
     * Methode de mise en forme de la trame à envoyer
     * @param data
     * @return
     */
    public byte[] toFrame(byte[] data){
        mFrame = new LinkedList<Byte>();
        int framelength=data.length+5;
        int lengthData=data.length;

        this.mLength[1]= (byte) (lengthData&0x00FF);
        this.mLength[0]= (byte) (lengthData&0xFF00);

        this.mFrame.add(0,HEADER);

        this.mFrame.add(1,mLength[0]);
        this.mFrame.add(2,mLength[1]);

        for (int i=0; i<data.length;i++ ){
            this.mFrame.add(i+3,data[i]);
        }
        this.mControl = control(data);
        this.mFrame.add(framelength-2,mControl);
        this.mFrame.add(framelength-1,TAIL);
        //control
        int addValue=0;
        for(int i=3;i<this.mFrame.size()-2;i++ ){
            if (this.mFrame.get(addValue+i)==TAIL||this.mFrame.get(addValue+i)==HEADER||this.mFrame.get(addValue+i)==ESC){
                this.mFrame.set(addValue+i, (byte) (this.mFrame.get(addValue+i)+(byte)0x06));
                this.mFrame.add(addValue+i,(byte)0x06);
                addValue+=1;
            }
        }

        byte[] byteArray= new byte[this.mFrame.size()];
        for(int i=0;i<this.mFrame.size();i++){
            byteArray[i]=this.mFrame.get(i);
        }
        return byteArray;




    }

    /**
     * Creation de la valeur de controle
     * @return Retourne le byte de controle
     */
    private byte control(byte[] data) {
        int nb_final = 0;
        for (int i=0;i<data.length;i++){
            nb_final+=(int)data[i];
        }
        nb_final+=(int)(mLength[0]+mLength[1]);

        byte byte_final = (byte) ((((nb_final)^(0xFF))%256)+1 );


        return byte_final;
    }
}
