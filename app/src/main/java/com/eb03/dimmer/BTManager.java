package com.eb03.dimmer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Classe de gestion du tranceiver bluetooth
 */
public class BTManager extends Transceiver{
    private static final UUID MY_UUID= UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothAdapter mAdapter;
    private BluetoothSocket mSocket=null;
    private ConnectThread mConnectThread = null;
    private WrittingThread mWrittingThread=null;
    private ByteRingBuffer mBuffer;

    /**
     * Constructeur de la classe BTManager, permettant d'associer un BluetoothAdapter
     * @param adapter : BluetoothAdapter en question
     */
    public BTManager(BluetoothAdapter adapter) {
        mAdapter=adapter;
    }

    @Override
    /**
     * Methode de connection a un periphérique bluetooth, a partir de son ID
     * Cette methode permet aussi, l'initialisation du Thread permettant l'ecriture dans le buffer circulaire
     */
    public void connect(String id) {
        BluetoothDevice device= BluetoothAdapter.getDefaultAdapter().getRemoteDevice(id);
        disconnect();
        mAdapter= BluetoothAdapter.getDefaultAdapter();
        attachFrameProcessor(new FrameProcessor());
        mConnectThread=new ConnectThread(device);
        setState(STATE_CONNECTING);
        mConnectThread.start();

    }

    @Override
    /**
     * Methode de deconnection d'un periphérique bluetooth
     */
    public void disconnect() {
    }

    @Override
    /**
     * Methode d'envoi de donnée
     */
    public void send(byte[] b) {
        try{
            byte[] frame = mFrameProcessor.toFrame(b);
            mWrittingThread.write(frame);
            System.out.println(frame);

        } catch (Exception e) {

            e.printStackTrace();
        }

    }


    /**
     * Classe du Thread de connection au device
     */
    private class ConnectThread extends Thread{
        public ConnectThread(BluetoothDevice device){
            try {
                mSocket=device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            mAdapter.cancelDiscovery();
            try {
                mSocket.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mConnectThread=null;
            startReadWriteThread();
        }
    }

    private void startReadWriteThread(){
        //instanciation d'un tread de lecture
        Log.i("ConnectThread","Thread WrittingThread lancé");
        mWrittingThread=new WrittingThread(mSocket);
        mWrittingThread.start();
        setState(STATE_CONNECTED);
    }
    /**
     * Classe du Thread d'ecriture vers le device
     */
    private class WrittingThread extends Thread{
        private OutputStream mOutStream;
        private Object MessageConstants;



        public WrittingThread(BluetoothSocket mSocket) {
            try {
                mOutStream=mSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mBuffer = new ByteRingBuffer(1024);
        }

        @Override
        public void run() {
            //boucle while qui surveille que le socket est pas vide
            while(mSocket!=null){
                try {
                    if (mBuffer.bytesToRead()>0){
                        byte[] bufferSent = new byte[1024];
                        bufferSent=mBuffer.getAll();
                        mOutStream.write(bufferSent);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
               }
            }
        }

        public void write(byte[] frame) {
            try{
                mBuffer.put(frame);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public ByteRingBuffer getBuffer() {
            return mBuffer;
        }
    }
}
