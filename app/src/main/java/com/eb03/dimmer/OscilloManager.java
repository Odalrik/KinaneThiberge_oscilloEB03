package com.eb03.dimmer;

public final class OscilloManager {

    private static OscilloManager instance;
    private Transceiver mTransceiver;

    private OscilloManager() {

    }

    /**
     * Instanciation du singleton
     */
    public static OscilloManager getInstance() {
        if (instance == null) {
            instance = new OscilloManager();
        }
        return instance;
    }

    /**
     * Envoi des donnees a chaque appel de cette methode
     * @param alpha
     */
    public void setCalibrationDutyCycle(float alpha){
        System.out.println(alpha);
        byte[] sendData={(byte)0x0A,(byte)(alpha*100)};
        mTransceiver.send(sendData);
    }

    /**
     * Passage de l'id du periphérique bluetooth appairé au tranceiver
     * @param id : id du periphérique bluetooth
     */
    public void connectTranceiver (String id ){mTransceiver.connect(id);}
    public void attachTranceiver(Transceiver transceiver){
        mTransceiver=transceiver;
    }
    public void detachTranceiver(){
        mTransceiver=null;
    }

    public Transceiver getmTransceiver() {
        return mTransceiver;
    }
}