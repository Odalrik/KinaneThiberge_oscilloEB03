package com.eb03.dimmer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import static android.os.Build.VERSION.SDK_INT;

/**
 * Classe de l'activité principale
 */
public class MainActivity extends AppCompatActivity {

    private final static int BT_CONNECT_CODE = 1;
    private final static int PERMISSIONS_REQUEST_CODE =0;
    private final static String[] BT_DANGEROUS_PERMISSIONS=new String[]{Manifest.permission.ACCESS_FINE_LOCATION};
    private TextView status;
    private TextView mValue;
    private boolean mState=false;

    private int progress;

    private OscilloManager mOscilloM;
    private BTManager btManager;


    private CustomView mCircleCustomView;
    private TextView mTv;
    private int mSliderValue=0;

    @Override
    /**
     * Sauvegarde des l'etat de l'application
     */
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("key_mValue", mSliderValue);;
    }

    @Override
    /**
     * Restauration de l'etat de l'application
     */
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mSliderValue = savedInstanceState.getInt("key_mValue", 0);
        mTv.setText(String.format("%d",(int)mSliderValue)+"%");
    }


    @Override
    /**
     * Methode de creation des elements graphique
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        status=findViewById(R.id.status);
        mOscilloM=OscilloManager.getInstance();

        //////////////////// ajout custom view ////////////////

        if (savedInstanceState != null) {
            mSliderValue = savedInstanceState.getInt("key_mValue", 0);
        } else {
            mSliderValue = 0;
        }

        mTv=findViewById(R.id.tv);

        mCircleCustomView=findViewById(R.id.CustomView);
        mCircleCustomView.setmCustomViewChangeListener(new CustomView.CustomViewChangeListener(){

            @Override
            /**
             * gestion de l'appuis lors d'un clic simple sur le slider
             */
            public void onChange(float value) {
                mTv.setText(String.format("%d",(int)value)+"%");
                mSliderValue=(int)value;
                mOscilloM.setCalibrationDutyCycle((float)value/100);
            }

            /**
             * gestion de l'appuis lors d'un double clic sur le slider
             */
            public void onDoubleClick(float value) {
                mTv.setText(String.format("%d",(int)value)+"%");
                mSliderValue=(int)value;
                mOscilloM.setCalibrationDutyCycle((float)0);
            }

        });
        verifyBtRight();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int menuItem = item.getItemId();
        switch(menuItem){
            case R.id.connect:
                Intent BTConnect;
                BTConnect = new Intent(this,BTConnectActivity.class);
                startActivityForResult(BTConnect,BT_CONNECT_CODE);
        }
        return true;
    }

    /**
     * Verrifie les droits : ici les droits bluetooth
     */
    private void verifyBtRight(){
        if(BluetoothAdapter.getDefaultAdapter()==null){
            Toast.makeText(this,"cette appli nécessite un adaptateur bluetooth",Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        if (SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_DENIED){
                requestPermissions(BT_DANGEROUS_PERMISSIONS,PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    @Override
    /**
     * demande de permission
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==PERMISSIONS_REQUEST_CODE){
            if(grantResults[0]== PackageManager.PERMISSION_DENIED){
                Toast.makeText(this, "Bluetooth permissions required", Toast.LENGTH_LONG).show();
                finish();
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case BT_CONNECT_CODE:
                if (resultCode == RESULT_OK) {
                    String address = data.getStringExtra("device");;
                    status.setText(address);
                    mState=true;
                }
                break;
            default:
        }
    }


}



