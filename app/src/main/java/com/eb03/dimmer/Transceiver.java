package com.eb03.dimmer;

/**
 * Classe abstraite integrant toutes les fonctionnalitées d'un tranceiver
 */
public abstract class Transceiver {
    public static final int STATE_NOT_CONNECTED = 0;
    public static final int STATE_CONNECTING = 1;
    public static final int STATE_CONNECTED = 2;
    private int mState = 0;
    protected TransceiverListener mTransceiverListener;
    protected FrameProcessor mFrameProcessor;


    /**
     * Setter de l'attribut mTransceiverListener
     */
    public void setTransceiverListener(TransceiverListener transceiverListener) {
        mTransceiverListener = transceiverListener;
    }
    /**
     * attache d'un mFrameProcessor
     */
    public void attachFrameProcessor(FrameProcessor frameProcessor) {
        mFrameProcessor = frameProcessor;
    }
    /**
     * Setter de l'attribut mFrameProcessor
     */
    public void detachFrameProcessor() {
        mFrameProcessor = null;
    }
    private FrameProcessor getFrameProcessor() {
        return mFrameProcessor;
    }
    private int getState() {
        return mState;
    }
    public void setState(int state) {
        mState = state;
        if (mTransceiverListener != null) {
            mTransceiverListener.onTransceiverStateChanged(state);
        }
    }
    /*********************************************************************************************
     *                                          Interfaces
     *********************************************************************************************/

    public interface TransceiverListener {
        void onTransceiverDataReceived();
        void onTransceiverStateChanged(int state);
        void onTransceiverConnectionLost();
        void onTransceiverUnableToConnect();
    }
    /*********************************************************************************************
     *                                     Méthodes abstraites
     *********************************************************************************************/
    public abstract void connect(String id);
    public abstract void disconnect();
    public abstract void send(byte[] b);

}