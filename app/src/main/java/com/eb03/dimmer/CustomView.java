package com.eb03.dimmer; /***************************
 * Autor : THIBERGE Kinane
 * Class : CustomView
 **************************/


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

public class CustomView<DEFAULT_BACK_CIRCLE_LAYER_DIAMETER> extends View {


    private  CustomViewChangeListener mCustomViewChangeListener = null;

    // si mState=true --> blocage a 0%
    // si mState=false --> blocage a 100%
    private boolean mState=true;

    //attributs de dimension (en pixels)
    private float mBackLayerCircleDiameter;
    private float mMiddleLayerCircleDiameter;
    private float mFrontLayerCircleDiameter;
    private float mProgressCircleDiameter;
    private float mProgressCircleLineWidth;
    private float mThickLineLength;
    private float mThickLineWidth;
    private float mThinLineLength;
    private float mThinLineWidth;

    // attributs de couleurs
    private int mDisabledColor;
    private int mBackLayerCircleColor;
    private int mMiddleLayerCircleColor;
    private int mFrontLayerCircleColor;
    private int mProgressCircleColor;
    private int mThickLineColor;
    private int mThinLineColor;

    // attributs de pinceaux
    private Paint mBackLayerCirclePaint;
    private Paint mMiddleLayerCirclePaint;
    private Paint mFrontLayerCirclePaint;
    private Paint mProgressCirclePaint;
    private Paint mThickLinePaint;
    private Paint mThinLinePaint;

    // Valeur du customView
    private float mValue = 0;

    // attributs de valeur
    private float mMin = 0;
    private float mMax = 100;

    // attribut d'activation
    private boolean mEnabled = true;

    // Dimensions par défaut
    private final static float DEFAULT_BACK_CIRCLE_LAYER_DIAMETER = 160;
    private final static float DEFAULT_MIDDLE_CIRCLE_LAYER_DIAMETER = (float) (DEFAULT_BACK_CIRCLE_LAYER_DIAMETER*0.90);
    private final static float DEFAULT_FRONT_CIRCLE_LAYER_DIAMETER = (float) (DEFAULT_BACK_CIRCLE_LAYER_DIAMETER*0.50);
    private final static float DEFAULT_PROGRESS_CIRCLE_DIAMETER = (DEFAULT_MIDDLE_CIRCLE_LAYER_DIAMETER+DEFAULT_FRONT_CIRCLE_LAYER_DIAMETER)/2;
    private final static float DEFAULT_PROGRESS_LINE_WIDTH = (DEFAULT_MIDDLE_CIRCLE_LAYER_DIAMETER-DEFAULT_FRONT_CIRCLE_LAYER_DIAMETER)/2;
    private final static float DEFAULT_THICK_LINE_LENGTH = (float) (DEFAULT_PROGRESS_LINE_WIDTH*0.8);
    private final static float DEFAULT_THICK_LINE_WIDTH = (float) (DEFAULT_PROGRESS_LINE_WIDTH*0.16);
    private final static float DEFAULT_THIN_LINE_LENGTH = (float) (DEFAULT_PROGRESS_LINE_WIDTH*0.6);
    private final static float DEFAULT_THIN_LINE_WIDTH = (float) (DEFAULT_PROGRESS_LINE_WIDTH*0.08);

    // espacement entre les traits et le cercle central (en pixel)
    private float mMinThick =dpToPixel((float) (DEFAULT_PROGRESS_LINE_WIDTH*0.2));;
    private float mMinThin =dpToPixel((float) (DEFAULT_PROGRESS_LINE_WIDTH*0.4));;

    // Dimensions par minimum
    private final static float MINI_FIRST_LAYER_DIAMETER = DEFAULT_MIDDLE_CIRCLE_LAYER_DIAMETER+20;
    private final static float MINI_SECOND_LAYER_DIAMETER = DEFAULT_FRONT_CIRCLE_LAYER_DIAMETER+20;

    //booleen d'etat du click
    private boolean mDoubleClick=false;
    private boolean mDisableMove=false;

    /**
     * Methode de conversion de dp en pixel
     * @param dp
     * @return la valeur en pixel
     */
    private float dpToPixel(float dp){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dp,getResources().getDisplayMetrics());
    }

    /**
     * Constructeur de la classe
     * @param context
     */
    public CustomView(Context context) {
        super(context);
        init(context,null);
        setSaveEnabled(true);
    }

    /**
     * Surcharge du constructeur de la classe
     * @param context
     * @param attrs
     */
    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
        setSaveEnabled(true);
    }

    /**
     * Initialisation des couleurs/dimensions en fonctions des parametres d'entree ou des parametres par defaut
     * @param context
     * @param attrs
     */
    private void init(Context context,@Nullable AttributeSet attrs){
        //init des Paint
        mBackLayerCirclePaint = new Paint();
        mMiddleLayerCirclePaint = new Paint();
        mFrontLayerCirclePaint = new Paint();
        mProgressCirclePaint = new Paint();
        mThickLinePaint  = new Paint();
        mThinLinePaint = new Paint();

        //init des couleurs par defaut
        mBackLayerCircleColor = ContextCompat.getColor(context,R.color.design_default_color_primary);
        mMiddleLayerCircleColor = ContextCompat.getColor(context,R.color.design_default_color_secondary);
        mFrontLayerCircleColor = ContextCompat.getColor(context,R.color.black);
        mProgressCircleColor = ContextCompat.getColor(context,R.color.purple_200);
        mThickLineColor = ContextCompat.getColor(context,R.color.purple_700);
        mThinLineColor = ContextCompat.getColor(context,R.color.purple_700);
        mDisabledColor = ContextCompat.getColor(context,R.color.grey);

        //init des dimensions par defaut
        mBackLayerCircleDiameter =dpToPixel(DEFAULT_BACK_CIRCLE_LAYER_DIAMETER);
        mMiddleLayerCircleDiameter=dpToPixel(DEFAULT_MIDDLE_CIRCLE_LAYER_DIAMETER);
        mFrontLayerCircleDiameter=dpToPixel(DEFAULT_FRONT_CIRCLE_LAYER_DIAMETER);
        mProgressCircleDiameter=dpToPixel(DEFAULT_PROGRESS_CIRCLE_DIAMETER);
        mProgressCircleLineWidth=dpToPixel(DEFAULT_PROGRESS_LINE_WIDTH);
        mThickLineLength=dpToPixel(DEFAULT_THICK_LINE_LENGTH);
        mThickLineWidth=dpToPixel(DEFAULT_THICK_LINE_WIDTH);
        mThinLineLength=dpToPixel(DEFAULT_THIN_LINE_LENGTH);
        mThinLineWidth=dpToPixel(DEFAULT_THIN_LINE_WIDTH);

        //si les parametres d'entrée sont non nul, reafectation des couleurs/dimensions
        if(attrs!=null){
            TypedArray attr = context.obtainStyledAttributes(attrs, R.styleable.CircleCustomView, 0, 0);

            //reafectation des couleurs
            mBackLayerCircleColor = attr.getColor(R.styleable.CircleCustomView_BackLayerCircleColor, mBackLayerCircleColor);
            mMiddleLayerCircleColor = attr.getColor(R.styleable.CircleCustomView_MiddleLayerCircleColor, mMiddleLayerCircleColor);
            mFrontLayerCircleColor = attr.getColor(R.styleable.CircleCustomView_FrontLayerCircleColor, mFrontLayerCircleColor);
            mProgressCircleColor = attr.getColor(R.styleable.CircleCustomView_ProgressCircleColor, mProgressCircleColor);
            mThickLineColor = attr.getColor(R.styleable.CircleCustomView_ThickLineColor, mThickLineColor);
            mThinLineColor = attr.getColor(R.styleable.CircleCustomView_ThinLineColor, mThinLineColor);

            //reafectation des dimensions
            mBackLayerCircleDiameter =(attr.getDimension(R.styleable.CircleCustomView_BackLayerCircleDiameter, mBackLayerCircleDiameter));
            mMiddleLayerCircleDiameter= (float) (mBackLayerCircleDiameter *0.90);
            mFrontLayerCircleDiameter=(float) (mBackLayerCircleDiameter *0.50);
            mProgressCircleDiameter=(mMiddleLayerCircleDiameter+mFrontLayerCircleDiameter)/2;
            mProgressCircleLineWidth=(mMiddleLayerCircleDiameter-mFrontLayerCircleDiameter)/2;
            mThickLineLength= (float) (mProgressCircleLineWidth*0.8);
            mThickLineWidth= (float) (mProgressCircleLineWidth*0.16);
            mThinLineLength= (float) (mProgressCircleLineWidth*0.6);
            mThinLineWidth= (float) (mProgressCircleLineWidth*0.08);
            mMinThick=(float) (mProgressCircleLineWidth*0.2);
            mMinThin=(float) (mProgressCircleLineWidth*0.4);
            mEnabled = attr.getBoolean(R.styleable.CircleCustomView_enabled, mEnabled);
            attr.recycle();
        }

        if(mEnabled){
            mBackLayerCirclePaint.setColor(mBackLayerCircleColor);
            mMiddleLayerCirclePaint.setColor(mMiddleLayerCircleColor);
            mFrontLayerCirclePaint.setColor(mFrontLayerCircleColor);
            mProgressCirclePaint.setColor(mProgressCircleColor);
            mThickLinePaint.setColor(mThickLineColor);
            mThinLinePaint.setColor(mThinLineColor);
        }else{
            mBackLayerCirclePaint.setColor(mDisabledColor);
            mMiddleLayerCirclePaint.setColor(mDisabledColor);
            mFrontLayerCirclePaint.setColor(mDisabledColor);
            mProgressCirclePaint.setColor(mDisabledColor);
            mThickLinePaint.setColor(mDisabledColor);
            mThinLinePaint.setColor(mDisabledColor);
        }

        //definition de la structure des traits
        mBackLayerCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mMiddleLayerCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mFrontLayerCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mProgressCirclePaint.setStyle(Paint.Style.STROKE);
        mThickLinePaint.setStyle(Paint.Style.STROKE);
        mThinLinePaint.setStyle(Paint.Style.STROKE);

        mThickLinePaint.setStrokeCap(Paint.Cap.ROUND);
        mThinLinePaint.setStrokeCap(Paint.Cap.ROUND);

        //definition de l'epaisseur des traits
        mProgressCirclePaint.setStrokeWidth(mProgressCircleLineWidth);
        mThickLinePaint.setStrokeWidth(mThickLineWidth);
        mThinLinePaint.setStrokeWidth(mThinLineWidth);
    }

    /**
     * Methode qui retourne la position du centre du CustomView en fonction de la largeur du plus grand cercle et des paddings
     * @return la position du centre du CustomView
     */
    private Point toPos(){
        int x,y;
        x = (int)(mBackLayerCircleDiameter /2)+getPaddingLeft();
        y=(int)(mBackLayerCircleDiameter /2)+getPaddingTop();
        return new Point(x,y);
    }

    /**
     * Methode qui retourne un point avec ces coordonnées en X et Y, en fonction d'un angle (en radian) et d'une distance (en pixel) par rapport au centre du CustomView
     * @param angle angle en radian
     * @param distance distance en pixel
     * @return un point et ces coordonnées en X,Y
     */
    private Point toPosLine(double angle, float distance){
        int x=0,y=0;
        x = (int)(Math.cos(angle)*((mFrontLayerCircleDiameter/2)+distance))+(int)(mBackLayerCircleDiameter /2)+getPaddingLeft();
        y = (int)(Math.sin(angle)*((mFrontLayerCircleDiameter/2)+distance))+(int)(mBackLayerCircleDiameter /2)+getPaddingTop();
        return new Point(x,y);
    }

    @Override
    /**
     * Methode de "dessin" du CustomView sur le canvas
     */
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //position du centre du cercle
        Point pc= toPos();

        //dessin des deux premiers cercles (Front et Middle)
        canvas.drawCircle(pc.x,pc.y, mBackLayerCircleDiameter /2,mBackLayerCirclePaint);
        canvas.drawCircle(pc.x,pc.y,mMiddleLayerCircleDiameter/2,mMiddleLayerCirclePaint);

        //dessin de l'arc de cercle dynamique
        final RectF ProgressCircle = new RectF();
        ProgressCircle.set((int)((mBackLayerCircleDiameter /2)+getPaddingLeft()-(mProgressCircleDiameter/2)),(int)((mBackLayerCircleDiameter /2)+getPaddingTop()-(mProgressCircleDiameter/2)),(int)((mBackLayerCircleDiameter /2)+getPaddingLeft()+(mProgressCircleDiameter/2)),(int)((mBackLayerCircleDiameter /2)+getPaddingTop()+(mProgressCircleDiameter/2)));
        canvas.drawArc(ProgressCircle, 270, (float) ((mValue*360)/100), false, mProgressCirclePaint);

        //dessin du front cercle
        canvas.drawCircle(pc.x,pc.y,mFrontLayerCircleDiameter/2,mFrontLayerCirclePaint);

        //dessin des traits autour du cercle
        double rModulo;
        Point p1,p2;
        //rotation autour d'un cercle trigo de centre pc, par pas de PI/8, si le resultat modulo pi/4 = 0 c'est un gros trait sinon c'est un petit trait
        for (double i=0;i<2*Math.PI;i+=(Math.PI/8) ){

            //problèmes d'arrondi ...
            if (i%(Math.PI/4)<0.000001 && i-(Math.PI/4)>0){
                rModulo=0;
            }else{
                rModulo=i%(Math.PI/4);
            }
            //dessin du gros trait
            if (rModulo==0){
                p1=toPosLine(i,mMinThick);
                p2=toPosLine(i,mThickLineLength);
                canvas.drawLine(p1.x,p1.y,p2.x,p2.y,mThickLinePaint);

                //dessin du petit trait
            }else{
                p1=toPosLine(i,mMinThin);
                p2=toPosLine(i,mThinLineLength);
                canvas.drawLine(p1.x,p1.y,p2.x,p2.y,mThinLinePaint);
            }
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int suggestedWidth, suggestedHeight; //dimensions souhaitées
        int width, height; //dimensions calculées en tenant compte des spec du conteneur
        suggestedHeight = (int) mBackLayerCircleDiameter +getPaddingTop()+getPaddingBottom();
        suggestedWidth = (int) mBackLayerCircleDiameter +getPaddingLeft()+getPaddingRight();

        width=resolveSize(suggestedWidth,widthMeasureSpec);
        height=resolveSize(suggestedHeight,heightMeasureSpec);

        setMeasuredDimension(width,height);

    }

    /**
     * Calcul du pourcentage atteint, en fonction de la position du doigt
     * @param point position du doigt
     * @return pourcentage du cercle a remplir
     */
    private float toValue(Point point){
        double ratio;
        double angle=0;

        //ratio de la position du point par rapport au centre du cercle
        float xPosRatio = (point.x-(getPaddingLeft()+(mBackLayerCircleDiameter /2)))/(mBackLayerCircleDiameter /2);
        float yPosRatio = -1*(point.y-(getPaddingTop()+(mBackLayerCircleDiameter /2)))/(mBackLayerCircleDiameter /2);

        //Bornage des ratio
        if(xPosRatio >1 ||xPosRatio <-1 ) xPosRatio =Math.signum(xPosRatio);
        if(yPosRatio >1 ||yPosRatio <-1 ) yPosRatio =Math.signum(yPosRatio);

        //calcul du pourcentage du cercle a remplir, en fonction de la position du point
        if (Math.signum(xPosRatio)==1 && Math.signum(yPosRatio)==1) {
            angle = Math.atan(xPosRatio / yPosRatio);
            if(mState==false)angle=2*Math.PI;
        }
        else if (Math.signum(xPosRatio)==1 && Math.signum(yPosRatio)==-1){
            angle=Math.PI/2 - Math.atan(yPosRatio/xPosRatio);
            mState=true;
        }
        else if (Math.signum(xPosRatio)==-1 && Math.signum(yPosRatio)==-1) {
            angle=Math.PI+Math.atan(xPosRatio/yPosRatio);
            mState=false;
        }
        else if (Math.signum(xPosRatio)==-1 && Math.signum(yPosRatio)==1) {
            angle=3*Math.PI/2 - Math.atan(yPosRatio/xPosRatio);
            if(mState==true)angle=0;

        }
        //conversion angle/pourcentage
        ratio=(angle/Math.PI)*50;
        return (float)ratio;
    }

    public void setValue(float v){
        mValue=v;
    }

    /********************************
     * gestion des evenements
     *******************************/

    public interface CustomViewChangeListener{
        void onChange(float value);
        void onDoubleClick(float value);

    }

    /**
     * setter du listener du CustomView
     * @param customViewChangeListener
     */
    public void setmCustomViewChangeListener(CustomViewChangeListener customViewChangeListener){
        mCustomViewChangeListener =customViewChangeListener;
    }

    /**
     * Evenement au touché du CustomView
     * @param event
     * @return true
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_MOVE:
                if(!mDisableMove){
                    //Recuperation du pourcentage en fonction de la position du curseur
                    mValue =toValue(new Point((int)event.getX(),(int)event.getY()));
                    //mise a jour du textview
                    if (mCustomViewChangeListener != null) mCustomViewChangeListener.onChange(mValue);
                    invalidate();
                    invalidate();
                }
                break;

            case MotionEvent.ACTION_DOWN:
                mDisableMove = true;
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mDisableMove = false;
                    }
                },100);
                if(mDoubleClick){
                    mState=true;
                    //Forcage de la valeur a 0 lors du doubleclick
                    mValue=mMin;
                    if (mCustomViewChangeListener != null) mCustomViewChangeListener.onDoubleClick(mValue);
                    invalidate();
                }else{
                    mDoubleClick=true;
                    postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mDoubleClick=false;
                        }
                    },500);
                }
                invalidate();

                break;

            default :
        }
        return true;
    }

    /*******************************
     * Sauvegarde des données
     ******************************/

    private static class SavedState extends BaseSavedState {
        int index;
        boolean mStateSave;

        SavedState(Parcelable superState) {
            super(superState);
        }

        @RequiresApi(api = Build.VERSION_CODES.Q)
        private SavedState(Parcel in) {
            super(in);
            index = in.readInt();
            mStateSave =in.readBoolean();
        }

        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(index);
            out.writeBoolean(mStateSave);
        }

        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            @RequiresApi(api = Build.VERSION_CODES.Q)
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };


    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState myState = new SavedState(superState);
        myState.index = (int) this.mValue;
        myState.mStateSave = this.mState;
        return myState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mValue = savedState.index;
        this.mState = savedState.mStateSave;
        requestLayout();
        invalidate();
    }

}
