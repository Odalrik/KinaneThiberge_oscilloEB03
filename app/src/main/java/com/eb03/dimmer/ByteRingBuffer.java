package com.eb03.dimmer;


import java.util.Arrays;

/**
 * class ByteRingBuffer : Classe d'implémentation du RingBuffer
 */
public class ByteRingBuffer extends Throwable {
    public byte[] mByteBuffer;
    private int mReadIndex;
    private int mWriteIndex;
    private int mlengthBuffer;
    private boolean mBufferIsEmpty;
    private boolean mBufferIsFull;

    /**
     * Constructeur de la classe ByteRingBuffer
     * @param intBufferLength : longueur du buffer
     */
    public ByteRingBuffer(int intBufferLength){
        mReadIndex=0;
        mWriteIndex=0;
        mlengthBuffer=intBufferLength;
        mBufferIsEmpty =true;
        mBufferIsFull =false;
        mByteBuffer = new byte[mlengthBuffer];
    }


    /**
     * Methode d'incrementation de l'index du buffer (se reinitialise l'intexe pour qu'il ne depasse pas la dimension du tableau)
     * @param number indice actuel à incrémenter
     * @return le nouvel indice
     */
    private int incrementNumber(int number){
        if (number+1>=mlengthBuffer){
            return 0;
        }else return (number+1);
    }

    /**
     * Methode de test d'incrementation de l'index du buffer avec la valeur a ne pas dépasser et et le nombre d'incrementation a réaliser
     * @param number indice actuel à incrémenter
     * @param increment inrementation a réaliser
     * @param notPassValue valeur a ne pas dépasser
     * @return le nouvel indice
     */
    private int testIncrementNumber(int number,int increment,int notPassValue){
        int numberIncremented=number;
        for (int i=0;i<increment;i++){
            numberIncremented=incrementNumber(numberIncremented);
            if (notPassValue==numberIncremented){
                mBufferIsFull=true;
                break;
            }
        }
        return numberIncremented;
    }

    /**
     * Methode d'insertion de byte dans le ring buffer
     * @param tByte byte a inserer
     * @throws Exception : en cas d'overflow
     */
    public void put(byte tByte) throws Exception {
        if (!mBufferIsFull) {
            mBufferIsEmpty =false;
            mByteBuffer[mWriteIndex] = tByte;
            mWriteIndex = incrementNumber(mWriteIndex);
            System.out.println(this);
            if(mWriteIndex == mReadIndex){
                mBufferIsFull=true;
            }
        } else throw (new Exception("OverflowException in Buffer")) ;
    }

    /**
     * Methode d'insertion de tableau byte dans le ring buffer
     * @param tByte tableau de byte a inserer
     * @throws Exception : en cas d'overflow
     */
    public void put(byte[] tByte) throws Exception {
        testIncrementNumber(mWriteIndex,tByte.length,mReadIndex);
        if (!mBufferIsFull ){

            mBufferIsEmpty =false;
            for (int i=0;i<tByte.length;i++){
                mByteBuffer[mWriteIndex]=tByte[i];
                mWriteIndex=incrementNumber(mWriteIndex);
            }
            if(mWriteIndex == mReadIndex){
                mBufferIsFull=true;
            }
            System.out.println(this);
        } else{
            throw (new Exception("OverflowException in Buffer")) ;
        }
    }

    /**
     * Methode de recuperation d'un byte stocké dans le ring buffer
     * @return le byte de l'index mReadIndex du ring buffer
     * @throws Exception en cas de buffer vide
     */
    public byte get() throws Exception {
        if (!mBufferIsEmpty){
            mBufferIsFull =false;
            byte returnedValue=mByteBuffer[mReadIndex];
            mReadIndex=incrementNumber(mReadIndex);
            if(mWriteIndex == mReadIndex){
                mBufferIsEmpty=true;
            }
            return returnedValue;
        }else{
            throw (new Exception("Nothing in Buffer")) ;
        }
    }

    /**
     * Methode de recuperation de tout les bytes stocké dans le ring buffer, cette methode met aussi l'indexe de lecture au niveau de l'indice d'ecriture
     * @return tous les bytes du ring buffer
     */
    public byte[] getAll(){
        int nbreValueToRead=bytesToRead();
        byte[] ByteBuffer = new byte[nbreValueToRead];

        for (int i=0;i<nbreValueToRead;i++){
            ByteBuffer[i]=mByteBuffer[mReadIndex];
            mReadIndex=incrementNumber(mReadIndex);
        }
        mBufferIsEmpty=true;
        return ByteBuffer;
    }

    /**
     * Methode de récupération du nombre de byte a lire dans le ring buffer
     * @return le nombre de byte a lire
     */
    public int bytesToRead(){
        int index=0;
        if (!mBufferIsEmpty || !mBufferIsFull){
            int testReadIndex=mReadIndex;
            while (mWriteIndex != testReadIndex){
                //System.out.println("mWriteIndex"+mWriteIndex+"mReadIndex"+testReadIndex );
                testReadIndex=incrementNumber(testReadIndex);
                index++;
            }
            return index;
        }else return 0;
    }

    @Override

    public String toString() {
        return "ByteRingBuffer{" +
                "mByteBuffer=" + Arrays.toString(mByteBuffer) +
                ", mReadIndex=" + mReadIndex +
                ", mWriteIndex=" + mWriteIndex +
                ", mlengthBuffer=" + mlengthBuffer +
                ", mBufferIsEmpty=" + mBufferIsEmpty +
                ", mBufferIsFull=" + mBufferIsFull +
                '}';
    }
}


